<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m191128_183945_add_columns_to_user_table extends Migration
{
    private $tableName = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'name', $this->string()->notNull());
        $this->addColumn($this->tableName, 'surname', $this->string()->notNull());
        $this->addColumn($this->tableName, 'patronymic', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'name');
        $this->dropColumn($this->tableName, 'surname');
        $this->dropColumn($this->tableName, 'patronymic');
    }
}
