<?php

use yii\db\Migration;

/**
 * Class m191128_182917_insert_records_into_subscription_table
 */
class m191128_182917_insert_records_into_subscription_table extends Migration
{
    private $tableName = 'subscription';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert($this->tableName, ['name', 'code'], [
            ['BOOM', 'boom'],
            ['Spotify', 'spotify'],
            ['TIDAL', 'tidal'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['code' => ['boom', 'spotify', 'tidal']]);
    }
}
