<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'username') ?>

    <?php echo $form->field($model, 'name') ?>

    <?php echo $form->field($model, 'surname') ?>

    <?php echo $form->field($model, 'patronymic') ?>

    <?php echo $form->field($model, 'email') ?>


  <div class="form-group">
      <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>

      <?= Html::a('Reset', '.', ['class' => 'btn btn-outline-secondary', 'role' => 'button']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
