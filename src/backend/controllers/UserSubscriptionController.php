<?php

namespace backend\controllers;

use common\models\UserSubscription;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class UserSubscriptionController
 * @package backend\controllers
 */
class UserSubscriptionController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'update' => ['class' => 'backend\models\UserSubscriptionUpdateAction'],
        ];
    }

    /**
     * AJAX валидация
     * @param int $idUser
     * @return mixed
     */
    public function actionValidate(int $idUser)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        if ($request->isPost && $request->post('ajax') !== null) {
            $userSubscriptions = UserSubscription::findAll(['fid_user' => $idUser]);
            $userSubscriptionsIds = array_column($userSubscriptions, 'id');

            $formData = $request->post('UserSubscription', []);
            $models = [];

            if ($formData) {
                foreach ($formData as $key => $item) {
                    $id = $item['id'];
                    $models[$key] = $id ? $userSubscriptions[array_search($id, $userSubscriptionsIds, false)]
                        : new UserSubscription();
                }
            }

            Model::loadMultiple($models, $formData, '');

            return ActiveForm::validateMultiple($models);
        }

        return [];
    }
}
