<?php

use common\models\User;
use yii\bootstrap\Collapse;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

  <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php echo Collapse::widget([
        'items' => [
            [
                'label' => Html::icon('search') . ' Поиск',
                'content' => $this->render('_search', ['model' => $searchModel]),
            ]
        ],
        'encodeLabels' => false
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            [
                'label' => 'ФИО',
                'value' => static function (User $model) {
                    return "$model->surname $model->name $model->patronymic";
                },
            ],
            'email:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]) ?>

    <?php Pjax::end(); ?>

</div>
