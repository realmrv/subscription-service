<?php

use yii\db\Migration;

/**
 * Class m191201_162119_alter_columns_in_user_table
 */
class m191201_162119_alter_columns_in_user_table extends Migration
{
    private $tableName = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'name', $this->string());
        $this->alterColumn($this->tableName, 'surname', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'name', $this->string()->notNull());
        $this->alterColumn($this->tableName, 'surname', $this->string()->notNull());
    }
}
