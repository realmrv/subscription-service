<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_subscription".
 *
 * @property int $id
 * @property int $fid_user
 * @property int $fid_subscription
 * @property int $end_date
 *
 * @property User $user
 * @property Subscription $subscription
 */
class UserSubscription extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fid_user', 'fid_subscription', 'end_date'], 'required'],
            [['fid_user', 'fid_subscription'], 'integer'],
            [['fid_user', 'fid_subscription'], 'unique', 'targetAttribute' => ['fid_user', 'fid_subscription'], 'message' => 'The user already has such a subscription'],
            ['end_date', 'filter', 'filter' => 'strtotime'],
            ['end_date', 'date', 'format' => 'php:U'],
            [['fid_subscription'], 'exist', 'skipOnError' => true, 'targetClass' => Subscription::class, 'targetAttribute' => ['fid_subscription' => 'id']],
            [['fid_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['fid_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fid_user' => 'Fid User',
            'fid_subscription' => 'Fid Subscription',
            'end_date' => 'End Date',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::class, ['id' => 'fid_subscription']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'fid_user']);
    }

    public function afterFind()
    {
        $this->end_date = date('d-m-Y 23:59:59', $this->end_date);

        parent::afterFind();
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->end_date = date('d-m-Y 23:59:59', $this->end_date);

        parent::afterSave($insert, $changedAttributes);
    }
}
