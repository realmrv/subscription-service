<?php

use yii\db\Migration;

class m191201_110043_create_unique_index_in_user_subscription_table extends Migration
{
    private $tableName = 'user_subscription';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            "idx-unique-$this->tableName-fid_user-fid_subscription",
            $this->tableName,
            'fid_user, fid_subscription',
            1
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex("idx-unique-$this->tableName-fid_user-fid_subscription", $this->tableName);
    }
}
