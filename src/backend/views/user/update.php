<?php

use common\models\Subscription;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $userSubscriptions common\models\UserSubscription[] */
/* @var $subscriptionMapped array */
/* @var $subscriptionCount int */

$this->title = 'Update User: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

  <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model')) ?>
    <?= $this->render('/user-subscription/_form', [
        'idUser' => $model->id,
        'models' => $userSubscriptions,
        'subscriptionCount' => Subscription::find()->count(),
        'subscriptionMapped' => ArrayHelper::map(Subscription::find()->all(), 'id', 'name'),
    ]) ?>

</div>
