let loader = {
  show: function (element) {
    if (typeof element === 'undefined') {
      element = $('body');
    }
    element.spin({position: 'fixed'});
    element.addClass('blocked');
  },
  hide: function (element) {
    if (typeof element === 'undefined') {
      element = $('body');
    }
    element.spin(false);
    element.removeClass('blocked');
  }
};

$(() => {
  $(document).ajaxSend(function (event, xhr, options) {
    if (options.showLoader || true) {
      loader.show();
    }
  }).ajaxComplete(function (event, xhr, options) {
    if (options.showLoader || true) {
      loader.hide();
    }
  }).ajaxError(function (event, jqXHR, ajaxSettings, thrownError) {
    if (jqXHR.status !== 0 && jqXHR.status !== 302) {
      bootbox.alert({
        size: 'large',
        title: 'Ошибка сервера: [readyState: ' + jqXHR.readyState + ', status: ' + jqXHR.status + ', statusText: ' + jqXHR.statusText + ']',
        message: jqXHR.responseText
      });
    }
  });
});