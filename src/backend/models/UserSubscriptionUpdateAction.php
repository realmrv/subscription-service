<?php

namespace backend\models;

use common\models\Subscription;
use common\models\UserSubscription;
use Exception;
use Throwable;
use Yii;
use yii\base\Action;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class UserSubscriptionUpdateAction
 * @package backend\models
 */
class UserSubscriptionUpdateAction extends Action
{
    /**
     * @param int $idUser
     * @return string
     * @throws NotFoundHttpException
     */
    public function run(int $idUser): string
    {
        $request = Yii::$app->request;

        if (!$request->isPost) {
            throw new NotFoundHttpException();
        }

        $userSubscriptions = UserSubscription::findAll(['fid_user' => $idUser]);
        $userSubscriptionsIds = array_column($userSubscriptions, 'id');

        $models = [];
        $formData = $request->post('UserSubscription', []);

        if ($formData) {
            $models = $this->prepareSubscriptions($models, $formData, $userSubscriptions, $userSubscriptionsIds);
            $userSubscriptionsIdsForm = array_filter(array_column($models, 'id'));
        }

        $models = $this->saveSubscriptions($models, array_diff($userSubscriptionsIds, $userSubscriptionsIdsForm ?? []));

        return Yii::$app->controller->renderPartial('_form', [
            'idUser' => $idUser,
            'models' => $models ?: [new UserSubscription()],
            'subscriptionCount' => Subscription::find()->count(),
            'subscriptionMapped' => ArrayHelper::map(Subscription::find()->all(), 'id', 'name'),
        ]);
    }

    /**
     * @param UserSubscription[] $models
     * @param array $formData
     * @param UserSubscription[] $userSubscriptions
     * @param array $userSubscriptionsIds
     * @return UserSubscription[]
     */
    private function prepareSubscriptions(
        array $models,
        array $formData,
        array $userSubscriptions,
        array $userSubscriptionsIds
    ): array
    {
        foreach ($formData as $key => $item) {
            $id = $item['id'];
            $models[$key] = $id
                ? $userSubscriptions[array_search($id, $userSubscriptionsIds, false)]
                : new UserSubscription();
        }

        Model::loadMultiple($models, $formData, '');

        return $models;
    }

    /**
     * @param UserSubscription[] $models
     * @param null $deletedSubscriptionsIds
     * @return UserSubscription[]
     * @throws Exception
     */
    private function saveSubscriptions(array $models, $deletedSubscriptionsIds = null): array
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $deletedSubscriptionsIds && UserSubscription::deleteAll(['id' => $deletedSubscriptionsIds]);

            if ($models) {
                foreach ($models as $model) {
                    $model->save();
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (Throwable $e) {
            $transaction->rollBack();
        }

        return $models;
    }
}
