<?php

namespace api\controllers;

use common\models\User;
use common\models\UserForm;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class UserController
 * @package app\controllers
 */
class UserController extends ActiveController
{
    public $modelClass = 'common\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::class,
            'auth' => function ($username, $password) {
                return (AUTH_LOGIN === $username && AUTH_PASSWORD === $password)
                    ? User::findOne(['username' => 'api'])
                    : null;
            },
        ];

        return $behaviors;
    }

    public function actions()
    {
        return null;
    }

    /**
     * @return array
     */
    public function actionIndex(): array
    {
        $users = User::find()->cache()->all();
        $result = [];

        foreach ($users as $user) {
            $result[] = $this->prepareUserData($user);
        }

        return $result;
    }

    /**
     * @param User $user
     * @return array
     */
    private function prepareUserData(User $user): array
    {
        $result['id'] = $user->primaryKey;
        $result['username'] = $user->username;
        $result['fio'] = trim("$user->surname $user->name $user->patronymic");

        $result['subscriptions'] = [];
        foreach ($user->subscription as $item) {
            $result['subscriptions'][] = [$item->subscription->name, $item->end_date];
        }

        return $result;
    }

    /**
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): array
    {
        $user = User::findOne($id);
        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        return $this->prepareUserData($user);
    }

    /**
     * @param int $id
     * @return array
     * @throws BadRequestHttpException
     * @throws InvalidConfigException
     */
    public function actionUpdate(int $id): array
    {
        $userForm = new UserForm(compact('id'));

        if (!$userForm->load(Yii::$app->getRequest()->getBodyParams(), '')) {
            throw new BadRequestHttpException('Загрузить данные не удалось');
        }

        $userForm->save();

        return $userForm->errors;
    }
}
