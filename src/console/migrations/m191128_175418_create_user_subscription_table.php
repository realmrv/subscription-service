<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_subscription}}`.
 */
class m191128_175418_create_user_subscription_table extends Migration
{
    private $tableName = 'user_subscription';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'fid_user' => $this->integer()->notNull(),
            'fid_subscription' => $this->integer()->notNull(),
            'end_date' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex(
            "idx-$this->tableName-fid_user",
            $this->tableName,
            'fid_user'
        );

        $this->addForeignKey(
            "fk-$this->tableName-fid_user",
            $this->tableName,
            'fid_user',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            "idx-$this->tableName-fid_subscription",
            $this->tableName,
            'fid_subscription'
        );

        $this->addForeignKey(
            "fk-$this->tableName-fid_subscription",
            $this->tableName,
            'fid_subscription',
            'subscription',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            "fk-$this->tableName-fid_user",
            $this->tableName
        );

        $this->dropIndex(
            "idx-$this->tableName-fid_user",
            $this->tableName
        );

        $this->dropForeignKey(
            "fk-$this->tableName-fid_subscription",
            $this->tableName
        );

        $this->dropIndex(
            "idx-$this->tableName-fid_subscription",
            $this->tableName
        );

        $this->dropTable($this->tableName);
    }
}
