<?php

namespace common\models;

use RuntimeException;
use yii\base\Model;

/**
 * Class UserForm
 * @package backend\models
 */
class UserForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $name;
    public $surname;
    public $patronymic;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'safe'],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'on' => 'change', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'on' => 'change', 'message' => 'This email address has already been taken.'],

            ['password', 'string', 'min' => 6],

            ['name', 'required', 'when' => static function(self $model) {
                return $model->surname || $model->patronymic;
            }, 'enableClientValidation' => false],
            ['surname', 'required', 'when' => static function(self $model) {
                return $model->name || $model->patronymic;
            }, 'enableClientValidation' => false],
            ['patronymic', 'required', 'when' => static function(self $model) {
                return $model->name || $model->surname;
            }, 'enableClientValidation' => false],
            [['name', 'surname', 'patronymic'], 'string', 'max' => 255],
            [['name', 'surname', 'patronymic'], 'trim'],
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = User::findOne($this->id);

        if (!$user) {
            throw new RuntimeException("Пользователь с ID $this->id не найден");
        }

        $user->username = $this->username;
        $user->email = $this->email;
        $user->name = $this->name;
        $user->surname = $this->surname;
        $user->patronymic = $this->patronymic;

        if ($this->password) {
            $user->setPassword($this->password);
        }

        return $user->save();
    }
}
