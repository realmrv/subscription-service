<?php

namespace backend\assets;

use Yii;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;

/**
 * Class BootBoxAsset
 * @package newcontact\contactLoader
 */
class BootBoxAsset extends AssetBundle
{
    public $sourcePath = '@npm/bootbox';
    public $js = [YII_ENV_PROD ? 'dist/bootbox.min.js' : 'bootbox.js'];
    public $depends = [BootstrapPluginAsset::class];

    public static function overrideSystemConfirm()
    {
        Yii::$app->view->registerJs('
            yii.confirm = function(message, ok, cancel) {
                bootbox.confirm(message, function(result) {
                    if (result) { !ok || ok(); } else { !cancel || cancel(); }
                });
            }
        ');
    }
}
