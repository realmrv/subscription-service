<?php

use kartik\select2\Select2;
use trntv\yii\datetime\DateTimeWidget;
use unclead\multipleinput\TabularColumn;
use unclead\multipleinput\TabularInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $idUser common\models\UserForm */
/* @var $models common\models\UserSubscription[] */
/* @var $subscriptionMapped array */
/* @var $subscriptionCount int */
?>
<div class="user-subscription-form">

    <?php Pjax::begin(['enablePushState' => false, 'id' => 'user-subscription-form-pjax']) ?>
    <?php $form = ActiveForm::begin([
        'id' => 'user-subscription-form',
        'action' => ['user-subscription/update', 'idUser' => $idUser],
        'validationUrl' => ['user-subscription/validate', 'idUser' => $idUser],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnChange' => false,
        'validateOnBlur' => false,
        'options' => ['data-pjax' => true, 'enctype' => 'multipart/form-data', 'showLoader' => 'true'],
    ]); ?>

    <?= TabularInput::widget([
        'id' => 'user-subscription-schedule',
        'models' => $models,
        'form' => $form,
        'max' => $subscriptionCount,
        'min' => 0,
        'modelClass' => 'common\models\UserSubscription',
        'attributeOptions' => [
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnChange' => false,
            'validateOnBlur' => false,
        ],
        'columns' => [
            [
                'name' => 'id',
                'type' => TabularColumn::TYPE_HIDDEN_INPUT,
            ],
            [
                'name' => 'fid_user',
                'type' => TabularColumn::TYPE_HIDDEN_INPUT,
                'defaultValue' => $idUser
            ],
            [
                'name' => 'fid_subscription',
                'type' => Select2::class,
                'title' => 'Subscription',
                'enableError' => true,
                'attributeOptions' => [
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnChange' => true,
                    'validateOnBlur' => true,
                ],
                'options' => [
                    'data' => $subscriptionMapped,
                ],
            ],
            [
                'name' => 'end_date',
                'type' => DateTimeWidget::class,
                'title' => 'End date',
                'enableError' => true,
                'attributeOptions' => [
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'validateOnChange' => true,
                    'validateOnBlur' => true,
                ],
                'options' => [
                    'phpDatetimeFormat' => 'd-m-Y 23:59:59',
                    'momentDatetimeFormat' => 'DD-MM-YYYY',
                    'clientOptions' => [
                        'locale' => 'ru',
                        'useCurrent' => false,
                        'allowInputToggle' => true,
                        'ignoreReadonly' => true,
                        'showClose' => true,
                        'showClear' => true,
                        'showTodayButton' => true,
                    ],
                    'options' => ['readonly' => true],
                ]
            ],
        ]
    ]) ?>
  <div class="form-group">
      <?= Html::submitButton('Update', ['class' => 'btn btn-success']) ?>
  </div>
    <?php ActiveForm::end() ?>
    <?php Pjax::end() ?>
</div>
